/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#include "ipc_transmit_mock.h"
#include "module_common_ipc_proxy.h"

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {
using namespace testing;
TEST(ModuleCommonIpcProxyTest, SeCommonIsServiceAvailableProxyTestInvalidInput0)
{
    ResultCode ret = SeCommonIsServiceAvailableProxy(nullptr, SERVICE_ID_PIN_AUTH, nullptr);
    EXPECT_EQ(ret, INVALID_PARA_NULL_PTR);
}

TEST(ModuleCommonIpcProxyTest, SeCommonIsServiceAvailableProxyTestInvalidInput1)
{
    ResultCode ret = SeCommonIsServiceAvailableProxy(IpcTransmitMock::MockIpcTransmit, SERVICE_ID_PIN_AUTH, nullptr);
    EXPECT_EQ(ret, INVALID_PARA_NULL_PTR);
}

TEST(ModuleCommonIpcProxyTest, SeCommonIsServiceAvailableProxyTestInvalidOutput1)
{
    auto transmit = [](uint32_t cmd, const uint8_t *data, uint32_t dataLen, uint8_t *reply,
                        uint32_t *replyLen) -> ResultCode {
        EXPECT_EQ(cmd, SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, CMD_IS_SERVICE_AVAILABLE));
        EXPECT_EQ(dataLen, sizeof(uint32_t));
        EXPECT_EQ(SERVICE_ID_PIN_AUTH, *(const uint32_t *)data);
        EXPECT_NE(reply, nullptr);
        EXPECT_EQ(*replyLen, sizeof(SeServiceStatus));
        return RES_APDU_FMT_ERR;
    };

    IpcTransmitMock mock;
    EXPECT_CALL(mock, IpcTransmit).Times(Exactly(1)).WillOnce(Invoke(transmit));
    SeServiceStatus status;
    ResultCode ret = SeCommonIsServiceAvailableProxy(IpcTransmitMock::MockIpcTransmit, SERVICE_ID_PIN_AUTH, &status);
    EXPECT_EQ(ret, RES_APDU_FMT_ERR);
}

TEST(ModuleCommonIpcProxyTest, SeCommonSetServiceConfigurationProxyInvalidInput0)
{
    auto ret = SeCommonSetServiceConfigurationProxy(nullptr, SERVICE_ID_PIN_AUTH, 0, nullptr);
    EXPECT_EQ(ret, INVALID_PARA_NULL_PTR);
}

TEST(ModuleCommonIpcProxyTest, SeCommonSetServiceConfigurationProxyInvalidInput1)
{
    auto ret = SeCommonSetServiceConfigurationProxy(IpcTransmitMock::MockIpcTransmit, SERVICE_ID_PIN_AUTH, 0, nullptr);
    EXPECT_EQ(ret, INVALID_PARA_NULL_PTR);
}

TEST(ModuleCommonIpcProxyTest, SeCommonSetServiceConfigurationProxyInvalidOutput1)
{
    auto transmit = [](uint32_t cmd, const uint8_t *data, uint32_t dataLen, uint8_t *reply,
                        uint32_t *replyLen) -> ResultCode {
        EXPECT_EQ(cmd, SERVICE_WITH_CMD_ID(SERVICE_ID_SEC_STORAGE, CMD_SET_SERVICE_CONFIG));
        EXPECT_EQ(dataLen, sizeof(SeServiceConfig) + sizeof(uint32_t) + sizeof(uint32_t));
        EXPECT_NE(data, nullptr);
        EXPECT_EQ(reply, nullptr);
        EXPECT_EQ(replyLen, nullptr);
        return RES_APDU_FMT_ERR;
    };

    IpcTransmitMock mock;
    EXPECT_CALL(mock, IpcTransmit).Times(Exactly(1)).WillOnce(Invoke(transmit));

    SeServiceConfig config = {0, {0}};

    auto ret =
        SeCommonSetServiceConfigurationProxy(IpcTransmitMock::MockIpcTransmit, SERVICE_ID_SEC_STORAGE, 0, &config);
    EXPECT_EQ(ret, RES_APDU_FMT_ERR);
}
} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS