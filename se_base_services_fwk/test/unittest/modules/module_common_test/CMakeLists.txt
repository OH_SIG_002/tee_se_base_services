include(GoogleTest)

add_executable(module_common_test src/module_common_core_test.cpp src/module_common_ipc_proxy_test.cpp)

target_include_directories(module_common_test PRIVATE inc)
target_include_directories(module_common_test PRIVATE ${SE_BASE_SERVICES_DEFAULT_INC})
target_compile_options(module_common_test PRIVATE ${SE_BASE_SERVICES_DEFAULT_CC})
target_link_libraries(module_common_test PRIVATE se_base_services_fwk_obj)
target_link_libraries(module_common_test PRIVATE securec)
target_link_libraries(module_common_test PRIVATE se_base_services_defines)
target_link_libraries(module_common_test PRIVATE se_apdu_core_obj)
target_link_libraries(module_common_test PRIVATE se_modules_common_ipc_proxy_obj)
target_link_libraries(module_common_test PRIVATE se_modules_common_ipc_stub_obj)
target_link_libraries(module_common_test PRIVATE logger_obj)
target_link_libraries(module_common_test PRIVATE parcel_obj)
target_link_libraries(module_common_test PRIVATE test_helpers_obj)
target_link_libraries(module_common_test PRIVATE se_base_test_mocks_obj)
target_link_libraries(module_common_test PRIVATE GTest::gmock_main)

gtest_discover_tests(module_common_test)
