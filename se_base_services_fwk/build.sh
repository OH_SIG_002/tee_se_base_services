#!/bin/bash

# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


set -e

project_dir=$(realpath $(dirname $0))

clean() {
    rm -fr build/
    rm -fr output/
}

build_test() {
    clean
    cmake \
        -B build \
        -S $project_dir \
        -DENABLE_TESTING=1 \
        -DENABLE_COVERAGE=1 \
        -DENABLE_FACTORY=1 \
        -DCMAKE_C_COMPILER=clang-12 \
        -DCMAKE_CXX_COMPILER=clang++-12 \
        -G Ninja

    cmake --build build
    ctest --test-dir build

    pushd build
    lcov --gcov-tool ~/bin/wrap_gcov --rc lcov_branch_coverage=1 -c -d frameworks -o se_base_frameworks
    genhtml --rc genhtml_branch_coverage=1 se_base_frameworks -o se_base_frameworks_report
    popd
}

case "$1" in
    "clean") clean ;;
    "test") build_test ;;
    *) build_test ;;
esac
