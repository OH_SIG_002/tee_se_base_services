/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CORE_SRC_RESPONSE_APDU_INNER_H
#define CORE_SRC_RESPONSE_APDU_INNER_H

#include <stdint.h>

#include "response_apdu.h"

// in ISO/IEC 7816-3 Response APDU structure defines with response body and response trailer
typedef struct ResponseApdu {
    uint32_t length; // total length, with response body and response trailer(sw1, sw2)
    uint8_t data[0];
} ResponseApdu;

#ifdef __cplusplus
extern "C" {
#endif

ResponseApdu *CreateResponseApdu(uint32_t bodyLength);

bool CheckResponseApdu(const ResponseApdu *responseApdu);

#ifdef __cplusplus
}
#endif

#endif // #ifndef CORE_SRC_RESPONSE_APDU_INNER_H