add_library(
    se_apdu_core_obj OBJECT
    src/card_channel.c
    src/apdu_utils.c
    src/command_apdu.c
    src/response_apdu.c
)

target_include_directories(se_apdu_core_obj PUBLIC inc)
target_include_directories(se_apdu_core_obj PRIVATE ${SE_BASE_SERVICES_DEFAULT_INC})

target_link_libraries(se_apdu_core_obj PRIVATE logger_obj)
target_link_libraries(se_apdu_core_obj PRIVATE se_base_services_defines)

target_compile_options(se_apdu_core_obj PRIVATE ${SE_BASE_SERVICES_DEFAULT_CC})

if(OPTION_SECUREC_INDEPENDENT)
    message(STATUS "se_apdu_core_obj use independent secure c.")
    target_link_libraries(se_apdu_core_obj PRIVATE securec_interface)
endif()

if(ENABLE_TESTING)
    target_include_directories(se_apdu_core_obj PUBLIC src)
endif()
