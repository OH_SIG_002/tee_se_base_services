/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INTERFACES_MODULE_SEC_STORAGE_DEFINES_H
#define INTERFACES_MODULE_SEC_STORAGE_DEFINES_H

#include <stdint.h>

#include "se_base_services_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct StorageDataBuffer {
    uint8_t *bufferData;
    uint32_t bufferSize;
} StorageDataBuffer;

typedef struct StorageAuthKey {
    uint8_t *keyData;
    uint32_t keySize;
} StorageAuthKey;

typedef enum FactoryResetLevel {
    LEVEL_USER_WIPE = 0,
    LEVEL_FACTORY_WIPE = 1,
    LEVEL_ALL_WIPE = 2,
} FactoryResetLevel;

typedef enum FactoryResetAuthAlgo {
    ALGO_NIST_P256 = 0,
    ALGO_SM2 = 1,
} FactoryResetAuthAlgo;

typedef enum SlotOperAlgo {
    ALGO_RAW_KEY = 0,
    ALGO_HKDF_SHA256 = 1,
    ALGO_HKDF_SM3 = 2,
} SlotOperAlgo;

typedef struct StorageUserModeConf {
    uint32_t pkResetable;
    uint32_t slotsResetable;
} StorageUserModeConf;

typedef struct StorageSlotAttr {
    SlotOperAlgo algo;
    FactoryResetLevel resetLevel;
    uint32_t size;
    uint32_t authOnRead;
    uint32_t authOnWrite;
    uint32_t authOnFree;
} StorageSlotAttr;

typedef enum SlotAllocStatus {
    SLOT_INIT = 0,
    SLOT_FREE = 1,
    SLOT_ALLOCATE = 2,
} SlotAllocStatus;

typedef struct StorageSlotStatus {
    uint32_t readErrCnt;
    uint32_t writeErrCnt;
    uint32_t freeErrCnt;
    SlotAllocStatus status;
    StorageSlotAttr slotAttr;
} StorageSlotStatus;

#ifdef __cplusplus
}
#endif

#endif // INTERFACES_MODULE_SEC_STORAGE_DEFINES_H