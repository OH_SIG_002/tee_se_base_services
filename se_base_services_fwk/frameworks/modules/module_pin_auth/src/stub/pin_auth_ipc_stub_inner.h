/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODE_MODULES_INC_PIN_AUTH_IPC_STUB_INNER_H
#define CODE_MODULES_INC_PIN_AUTH_IPC_STUB_INNER_H

#include "pin_auth_ipc_defines.h"
#include "pin_auth_ipc_stub.h"
#include "se_base_services_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

ResultCode ProcPinAuthCmdGetNumSlotsStub(PinAuthContext *context, SharedDataBuffer *buffer);

ResultCode ProcPinAuthCmdEnrollLocalStub(PinAuthContext *context, SharedDataBuffer *buffer);

ResultCode ProcPinAuthCmdAuthLocalStub(PinAuthContext *context, SharedDataBuffer *buffer);

ResultCode ProcPinAuthCmdEnrollRemoteStub(PinAuthContext *context, SharedDataBuffer *buffer);

ResultCode ProcPinAuthCmdAuthRemotePrepareStub(PinAuthContext *context, SharedDataBuffer *buffer);

ResultCode ProcPinAuthCmdAuthRemoteStub(PinAuthContext *context, SharedDataBuffer *buffer);

ResultCode ProcPinAuthCmdAuthRemoteAbortStub(PinAuthContext *context, SharedDataBuffer *buffer);

ResultCode ProcPinAuthCmdSetSelfDestructEnableStub(PinAuthContext *context, SharedDataBuffer *buffer);

ResultCode ProcPinAuthCmdGetFreezeStatusStub(PinAuthContext *context, SharedDataBuffer *buffer);

ResultCode ProcPinAuthCmdSetFreezePolicyStub(PinAuthContext *context, SharedDataBuffer *buffer);

ResultCode ProcPinAuthCmdGetFreezePolicyStub(PinAuthContext *context, SharedDataBuffer *buffer);

ResultCode ProcPinAuthCmdEraseSingleSlotStub(PinAuthContext *context, SharedDataBuffer *buffer);

ResultCode ProcPinAuthCmdEraseAllSlotsStub(PinAuthContext *context, SharedDataBuffer *buffer);

#ifdef __cplusplus
}
#endif

#endif // CODE_MODULES_INC_PIN_AUTH_IPC_STUB_INNER_H