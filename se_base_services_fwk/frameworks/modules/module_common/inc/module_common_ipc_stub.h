/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INTERFACES_SE_BASE_MODULE_COMMON_IPC_STUB_H
#define INTERFACES_SE_BASE_MODULE_COMMON_IPC_STUB_H

#include <stdint.h>

#include "card_channel.h"
#include "se_base_services_defines.h"

typedef struct SeCommonContext {
    uint32_t sender;
    CardChannel *channel;
} SeCommonContext;

#define INIT_SE_COMMON_CONTEXT(context, sender, channel) \
    (context).sender = (sender);                         \
    (context).channel = (channel)

typedef ResultCode SetBindingKey(CardChannel *channel, ServiceId sid, const uint8_t *initKey, uint32_t initKeyLength,
    uint32_t initKeyVersion);

typedef ResultCode DeleteInitKey(CardChannel *channel, const uint8_t *initKey, uint32_t initKeyLength,
    uint32_t initKeyVersion);

typedef struct SeManagerContext {
    SeCommonContext base;
    SetBindingKey *setBindingKey;
    DeleteInitKey *deleteInitKey;
} SeManagerContext;

#ifdef __cplusplus
extern "C" {
#endif

/* base */
ResultCode ProcessSeCommonCommandStub(SeCommonContext *context, uint32_t cmd, SharedDataBuffer *buffer);

/* manager */
ResultCode ProcessSeManagerCommandStub(SeManagerContext *context, uint32_t cmd, SharedDataBuffer *buffer);

#ifdef __cplusplus
}
#endif

#endif // INTERFACES_SE_BASE_MODULE_COMMON_IPC_STUB_H