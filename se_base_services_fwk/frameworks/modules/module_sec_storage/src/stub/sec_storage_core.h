/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODE_MODULES_INC_STORAGE_CORE_H
#define CODE_MODULES_INC_STORAGE_CORE_H

#include "card_channel.h"
#include "se_module_sec_storage_defines.h"
#include "sec_storage_ipc_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

ResultCode StorageSetFactoryResetAuthenticationKey(const CardChannel *channel, FactoryResetLevel level,
    FactoryResetAuthAlgo algo, const StorageAuthKey *key);

ResultCode StorageGetFactoryResetAuthenticationAlgo(const CardChannel *channel, FactoryResetLevel level,
    FactoryResetAuthAlgo *algo);

ResultCode StoragePrepareFactoryReset(const CardChannel *channel, uint8_t *nonce, uint32_t *length);

ResultCode StorageProcessFactoryReset(const CardChannel *channel, FactoryResetLevel level, const uint8_t *credential,
    uint32_t length);

ResultCode StorageSetToUserMode(const CardChannel *channel, const StorageUserModeConf *config);

ResultCode StorageIsSlotOperateAlgorithmSupported(const CardChannel *channel, SlotOperAlgo algo, uint32_t *available);

ResultCode StorageIsFactoryResetAlgorithmSupported(const CardChannel *channel, FactoryResetAuthAlgo algo,
    uint32_t *available);

ResultCode StorageAllocateSlot(const CardChannel *channel, uint8_t slotId, const StorageSlotAttr *slotAttr,
    const StorageAuthKey *slotKey);

ResultCode StorageGetSlotStatus(const CardChannel *channel, uint8_t slotId, StorageSlotStatus *status);

ResultCode StorageWriteSlot(const CardChannel *channel, uint8_t slotId, const StorageAuthKey *key,
    const StorageDataArea *area, const StorageDataBuffer *data);

ResultCode StorageReadSlot(const CardChannel *channel, uint8_t slotId, const StorageAuthKey *key,
    const StorageDataArea *area, StorageDataBuffer *data);

ResultCode StorageFreeSlot(const CardChannel *channel, uint8_t slotId, const StorageAuthKey *key);

ResultCode StorageSetAllSlotsSize(const CardChannel *channel, const uint16_t *slotSizeArray, uint32_t arrayLength);

ResultCode StorageGetAllSlotsSize(const CardChannel *channel, uint16_t *slotSizeArray, uint32_t *arrayLength);

ResultCode StorageGetSupportedAlgorithms(const CardChannel *channel, uint8_t *facResetAlgos, uint8_t *slotOperAlgos);

#ifdef __cplusplus
}
#endif

#endif // CODE_MODULES_INC_STORAGE_CORE_H