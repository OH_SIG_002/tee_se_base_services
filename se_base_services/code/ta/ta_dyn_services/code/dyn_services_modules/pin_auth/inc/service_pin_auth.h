/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DYN_SERVICE_INC_PIN_AUTH_H
#define DYN_SERVICE_INC_PIN_AUTH_H

#include <stdint.h>

#include "se_base_services_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

ResultCode PinAuthProcessCommand(uint32_t sender, uint32_t cmd, SharedDataBuffer *sharedData);

#ifdef __cplusplus
}
#endif

#endif // DYN_SERVICE_INC_PIN_AUTH_H