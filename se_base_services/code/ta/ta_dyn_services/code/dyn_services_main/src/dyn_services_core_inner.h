/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DYN_SERVICE_INC_COMMON_H
#define DYN_SERVICE_INC_COMMON_H

#include <stdint.h>

#include <tee_dynamic_srv.h>

#include "dyn_services_core.h"

#ifdef __cplusplus
extern "C" {
#endif

#define PUBLIC_API __attribute__((visibility("default")))
#define SERVICE_NAME "se_base_services"

#define MAX_BUFFER_LENGTH 1024

uint32_t GetRequestServiceId(tee_service_ipc_msg *msg);

uint32_t GetRequestCommand(tee_service_ipc_msg *msg);

uint8_t *RetainRequestBuffer(tee_service_ipc_msg *msg, uint32_t sndr, uint32_t *buffSize, uint32_t *buffMaxSize);

void ReleaseRequestBuffer(uint8_t *buffer, uint32_t bufferMaxSize);

TEE_Result tee_obj_init(void);
#ifdef __cplusplus
}
#endif

#endif // DYN_SERVICE_INC_COMMON_H