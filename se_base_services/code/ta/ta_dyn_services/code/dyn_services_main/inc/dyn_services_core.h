/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DYN_SERVICE_INC_CORE_H
#define DYN_SERVICE_INC_CORE_H

#include <stdint.h>

#include <tee_dynamic_srv.h>
#include <tee_log.h>
#include <tee_service_public.h>

#include "se_base_services_defines.h"
#include "services_fwk.h"

#ifdef __cplusplus
extern "C" {
#endif

#define SERVICE_REGISTER(sid, process, checker)                                    \
    __attribute__((constructor)) void ServiceRegister##sid(void)                   \
    {                                                                              \
        SLogTrace("ServiceFrameworkAddService sid = 0x%x with %s", sid, #process); \
        ServiceFrameworkAddService(sid, process, checker);                         \
    }

void ServiceProcessWithCommand(tee_service_ipc_msg *msg, uint32_t sndr, tee_service_ipc_msg_rsp *rsp);

ResultCode CheckClientPermission(uint32_t sndr, const TEE_UUID *uuidList, uint32_t listSize);

#ifdef __cplusplus
}
#endif

#endif // DYN_SERVICE_INC_CORE_H